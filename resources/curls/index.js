const axios = require('axios');
const { promisify } = require('util');
const faker = require('faker');
const sleep = promisify(setTimeout)


// querys consulta
const getEstado = async () => {
  while (1 == 1) {
    await axios.get('http://localhost:8081/videoclub/v1/estado');
    console.log('consumiendo recurso');
    // await sleep(500);
  }
}

const getPrestamos = async () => {
  while (1 == 1) {
    await axios.get('http://localhost:8081/videoclub/v1/prestamos');
    console.log('consumiendo recurso');
    // await sleep(500);
  }
}

const getBloqueos = async () => {
  while (1 == 1) {
    await axios.get('http://localhost:8081/videoclub/v1/bloqueos');
    console.log('consumiendo recurso');
    // await sleep(500);
  }
}

const getClientes = async () => {
  while (1 == 1) {
    await axios.get('http://localhost:8081/videoclub/v1/clientes');
    console.log('consumiendo recurso');
    // await sleep(500);
  }
}

const getDescuentos = async () => {
  while (1 == 1) {
    await axios.get('http://localhost:8081/videoclub/v1/Descuentos');
    console.log('consumiendo recurso');
    // await sleep(500);
  }
}

const getPrecios = async () => {
  while (1 == 1) {
    await axios.get('http://localhost:8081/videoclub/v1/precios');
    console.log('consumiendo recurso');
    // await sleep(500);
  }
}

const patchVideos = async () => {
  while (1 == 1) {
    await axios.patch('http://localhost:8081/videoclub/v1/videos/6094a2fab78a2a475781fc04', {
      'anio': 2005,
         'actores': [
          'Bob Odenkirk',
          'Aleksey Serebryakov',
          'Connie Nielsen',
          'Christopher Lloyd',
           faker.name.findName(),
       ]
    });
    console.log('editando videos');
  }
}

const crearUsuarios = async () => {

  while (1 == 1) {
    await axios.post('http://localhost:8081/videoclub/v1/clientes', {
      nombre: faker.name.findName(),
      celular: faker.datatype.number({ min: 6000000, max: 7999999 }),
      email: faker.internet.email(),
      fechaNacimiento: faker.date.between('1989-01-01', '2010-01-05'),
      nroDocumento: faker.datatype.number({ min: 1000000, max: 9999999 }),
      direccion: faker.address.streetAddress(),
      coordenadas: {
        latitud:  Number(faker.address.latitude()),
        longitud:  Number(faker.address.longitude()),
      },
      idbloqueo: []
    });
    console.log('creando usuarios');
  }
}


return Promise.all(
  [
     getEstado(),
    // getBloqueos(),
    // getClientes(),
    // getDescuentos(),
    // getPrecios(),
    // getPrestamos(),
    // // // edicion
     // patchVideos(),
    // creando
     creandoClientes(),
  ]
);