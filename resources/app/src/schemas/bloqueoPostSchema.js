module.exports = {
  title: 'bloqueo',
  type: 'object',
  additionalProperties: false,
  required: ['fecha', 'descripcion', 'estado'],
  properties: {
    fecha: {
      type: 'string',
      moment: {
        format: ['DD-MM-YYYY'],
      },
    },
    descripcion: {
      type: 'string',
    },
    estado: {
      type: 'string',
      enum: ['activo', 'desbloqueado'],
    },
  },
};
