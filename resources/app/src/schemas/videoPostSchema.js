module.exports = {
  title: 'Video',
  type: 'object',
  additionalProperties: false,
  required: ['generos', 'titulos', 'codigo'],
  properties: {
    titulos: {
      type: 'array',
      minLength: 1,
      items: {
        type: 'object',
        properties: {
          principal: {
            type: 'string',
          },
          titulo: {
            type: 'string',
          },
        },
      },
    },
    codigo: {
      type: 'string',
    },
    generos: {
      type: 'array',
      minLength: 1,
      items: {
        type: 'string',
      },
    },
    anio: {
      type: 'number',
    },
    nominaciones: {
      type: 'array',
      items: {
        type: 'string',
      },
    },
    oscars: {
      type: 'array',
      items: {
        type: 'string',
      },
    },
    actores: {
      type: 'array',
      items: {
        type: 'string',
      },
    },
    unidades: {
      type: 'integer',
    },
    bajas: {
      type: 'array',
      items: {
        fecha: '',
        razon: '',
      },
    },
  },
};
