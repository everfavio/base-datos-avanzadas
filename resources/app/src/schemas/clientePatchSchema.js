module.exports = {
  title: 'cliente',
  type: 'object',
  additionalProperties: false,
  properties: {
    nombre: {
      type: 'string',
    },
    celular: {
      type: 'string',
    },
    email: {
     type: 'string',
    },
    fechaNacimiento: {
      type: 'string',
    },
    nroDocumento: {
      type: 'string',
    },
    direccion: {
      type: 'string',
    },
    coordenadas: {
      type: 'object',
      required: ['latitud', 'longitud'],
      properties: {
        latitud: {
          type: 'number',
        },
        longitud: {
          type: 'number',
        },
      },
    },
    idBloqueo: {
      type: 'array',
      items: {
        type: 'string',
        minItems: 0,
      },
    },
  },
};
