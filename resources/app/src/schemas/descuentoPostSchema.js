module.exports = {
  title: 'descuento',
  type: 'object',
  additionalProperties: false,
  required: ['cantidad', 'descuento'],
  properties: {
    cantidad: {
      type: 'integer',
    },
    descuento: {
      type: 'number',
    },
  },
};
