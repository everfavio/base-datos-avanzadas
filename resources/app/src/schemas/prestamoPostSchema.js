module.exports = {
  title: 'prestamo',
  type: 'object',
  additionalProperties: false,
  required: [
    'peliculas',
    'cliente',
    'fechaPrestamo',
    'fechaDevolucion',
    'estado',
  ],
  properties: {
    peliculas: {
      type: 'array',
      minItems: 1,
      items: {
        type: 'string',
      },
    },
    cliente: {
      type: 'string',
    },
    fechaPrestamo: {
      type: 'string',
      moment: {
        format: ['DD-MM-YYYY'],
      },
    },
    fechaDevolucion: {
      type: 'string',
      moment: {
        format: ['DD-MM-YYYY'],
      },
    },
    estado: {
      type: 'string',
      enum: ['vigente', 'cerrado'],
    },
  },
};
