module.exports = {
  title: 'bloqueo',
  type: 'object',
  additionalProperties: false,
  properties: {
    fecha: {
      type: 'string',
      moment: {
        format: ['DD-MM-YYYY'],
      },
    },
    descripcion: {
      type: 'string',
    },
    estado: {
      type: 'string',
    },
  },
};
