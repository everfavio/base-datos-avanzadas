module.exports = {
  title: 'descuento',
  type: 'object',
  additionalProperties: false,
  properties: {
    descuento: {
      type: 'number',
    },
  },
};
