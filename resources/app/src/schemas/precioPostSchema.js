module.exports = {
  title: 'precio',
  type: 'object',
  additionalProperties: false,
  required: ['dia', 'costo'],
  properties: {
    dia: {
      type: 'integer',
    },
    costo: {
      type: 'integer',
    },
  },
};
