module.exports = {
  title: 'prestamo',
  type: 'object',
  additionalProperties: false,
  properties: {
    estado: {
      type: 'string',
      enum: ['vigente', 'cerrado'],
    },
  },
};
