const mongoose = require('mongoose');
const { descuentoModel } = require('../models');
const { mensajeError } = require('../utils/handleResponse');
const logger = require('../helpers/logger');
const schemaHelper = require('../helpers/schema_helper');
const descuentoPostSchema = require('../schemas/descuentoPostSchema');
const descuentoPatchSchema = require('../schemas/descuentoPatchSchema');
const CodeError = require('../errors/code_error');

module.exports = {
  listarDescuentos: async (req, res) => {
    try {
      const paginas = req.query.pagina ? parseInt(req.query.pagina, 10) : 1;
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;
      // const { keyword } = req.query;
      const listaDescuentosResult = await descuentoModel.paginate({}, { page: paginas, limit: limite });
      // if (keyword === null || keyword === undefined || keyword === '') {
      //   listaDescuentosResult = await descuentoModel.paginate({}, { page: paginas, limit: limite });
      // } else {
      //   listaDescuentosResult = await descuentoModel.paginate(
      //     {
      //     $or: [
      //       { dia: new RegExp(keyword.toUpperCase()) },
      //       ],
      //     },
      //     { page: paginas, limit: limite },
      //   );
      // }
      logger.info(`[__filename|]${listaDescuentosResult}`, 'retornandoDatos');
      res.status(200).json({
        descuentos: listaDescuentosResult.docs,
        totalDescuentos: listaDescuentosResult.total,
        limite: listaDescuentosResult.limit,
        pagina: listaDescuentosResult.page,
        totalPaginas: listaDescuentosResult.pages,
      });
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  buscarDescuento: async (req, res) => {
    try {
      let query = {};
      const idDescuentoParam = req.params.idDescuento;
      if (mongoose.Types.ObjectId.isValid(idDescuentoParam) === false) {
        query = {
          codigo: idDescuentoParam,
        };
      } else {
        query = {
          _id: idDescuentoParam,
        };
      }
      const resultado = await descuentoModel.findOne(query);
      if (resultado !== null) {
        res.status(200).json(resultado);
      }
      throw new CodeError('el descuento no se encuentra registrado', 0, 404);
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },

  crearDescuento: async (req, res) => {
    try {
      await schemaHelper.validate(descuentoPostSchema, req.body);
      const descuentoParam = req.body;
      const verificarExistencia = await descuentoModel.findOne({ cantidad: descuentoParam.cantidad });
      if (verificarExistencia !== null) {
         throw new CodeError('el descuento para esa cantidad ya se encuentra registrado', 0, 400);
      }
      const resultado = await descuentoModel(descuentoParam).save();
      // const hideID = resultado.forEach(item => { return delete item._id; });
      return res.status(200).json(resultado);
    } catch (error) {
      return mensajeError(res, error, error.errorCode);
    }
  },

  eliminarDescuento: async (req, res) => {
    try {
      const idDescuentoParam = req.params.idDescuento;
      const verificarExistencia = await descuentoModel.findById(idDescuentoParam);

      if (!verificarExistencia) {
        throw new CodeError('El descuento no se encuentra registrado.', 0, 404);
      }

      if (verificarExistencia.estado === 'Conectado') {
        throw new CodeError('No se puede eliminar ese descuento.', 0, 400);
      }

      await descuentoModel.remove({ _id: idDescuentoParam });
      res.status(204).json();
    } catch (error) {
      mensajeError(res, error, 400);
    }
  },

  editarDescuento: async (req, res) => {
    try {
      await schemaHelper.validate(descuentoPatchSchema, req.body);
      const descuento = await descuentoModel.findOne({ _id: req.params.idDescuento }).exec();
      if (descuento === null) {
        throw new CodeError('el descuento no se encuentra registrado', 0, 404);
      }
      await descuentoModel.findOneAndUpdate({ _id: req.params.idDescuento }, { $set: req.body });
      const nuevoDescuento = await descuentoModel.findOne({ _id: req.params.idDescuento });
      return res.status(200).json(nuevoDescuento);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
};
