const videosBL = require('./videosBL');
const clientesBL = require('./clientesBL');
const preciosBL = require('./preciosBL');
const descuentosBL = require('./descuentosBL');
const bloqueosBL = require('./bloqueosBL');
const prestamosBL = require('./prestamosBL');

module.exports = {
  videosBL,
  clientesBL,
  preciosBL,
  descuentosBL,
  bloqueosBL,
  prestamosBL,
};
