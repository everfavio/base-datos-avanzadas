const mongoose = require('mongoose');
const moment = require('moment');
const {
  prestamoModel,
  videoModel,
  clienteModel,
  descuentoModel,
  precioModel,
} = require('../models');
const { mensajeError } = require('../utils/handleResponse');
const logger = require('../helpers/logger');
const schemaHelper = require('../helpers/schema_helper');
const prestamoPostSchema = require('../schemas/prestamoPostSchema');
const prestamoPatchSchema = require('../schemas/prestamoPatchSchema');
const CodeError = require('../errors/code_error');


module.exports = {
  listarPrestamos: async (req, res) => {
    try {
      const paginas = req.query.pagina ? parseInt(req.query.pagina, 10) : 1;
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;
      let listaPrestamosResult;
      logger.info(`[__filename|]${listaPrestamosResult}`, 'retornandoDatos');
      const keyword = req.query;
      if (Object.keys(keyword).length === 0) {
        listaPrestamosResult = await prestamoModel.paginate({ estado: 'vigente' }, { page: paginas, limit: limite });
      } else {
        listaPrestamosResult = await prestamoModel.paginate(
          (req.query),
          { page: paginas, limit: limite },
        );
      }
      res.status(200).json({
        prestamos: listaPrestamosResult.docs,
        totalPrestamos: listaPrestamosResult.total,
        limite: listaPrestamosResult.limit,
        pagina: listaPrestamosResult.page,
        totalPaginas: listaPrestamosResult.pages,
      });
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  buscarPrestamo: async (req, res) => {
    try {
      let query = {};
      const idPrestamoParam = req.params.idPrestamo;
      if (mongoose.Types.ObjectId.isValid(idPrestamoParam) === false) {
        query = {
          dia: idPrestamoParam,
        };
      } else {
        query = {
          _id: idPrestamoParam,
        };
      }
      const resultado = await prestamoModel.findOne(query);
      if (resultado !== null) {
        res.status(200).json(resultado);
      }
      throw new CodeError('el prestamo no se encuentra registrado', 0, 404);
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  crearPrestamo: async (req, res) => {
    try {
      await schemaHelper.validate(prestamoPostSchema, req.body);
      const prestamoParam = req.body;
      // validar ids de pelicula
      const listaPeliculas = prestamoParam.peliculas;
      const peliculas = [];
      // buscando duplicados
      const findDuplicates = arr => arr.filter((item, index) => arr.indexOf(item) !== index);
      const duplicados = [...new Set(findDuplicates(listaPeliculas))];
      if (duplicados.length >= 1) {
        throw new CodeError(`existen ID de pelicula repetidos: ${duplicados.toString()}`, 0, 404);
      }

      // recuperando peliculas
      await Promise.all(listaPeliculas.map(async (element) => {
        if (mongoose.Types.ObjectId.isValid(element) === false) {
          throw new CodeError(`ID de pelicula ${element} no es un ID válido`, 0, 404);
        }
        const pelicula = await videoModel.findById(element);
        if (!pelicula) {
          throw new CodeError(`ID de pelicula ${element} no registrado`, 0, 404);
        }
        peliculas.push(pelicula);
      }));

      // validando cliente
      if (mongoose.Types.ObjectId.isValid(prestamoParam.cliente) === false) {
        throw new CodeError(`ID de cliente ${prestamoParam.cliente} no es un ID válido`, 0, 404);
      }
      const cliente = await clienteModel.findById(prestamoParam.cliente);
      if (!cliente) {
        throw new CodeError(`ID de cliente ${prestamoParam.cliente} no registrado`, 0, 404);
      }


      // verificando stock
      peliculas.map(element => {
        if (element.unidades === 0) {
          throw new CodeError(`la pelicula ${element._id} ya no tiene mas unidades en stock`, 0, 404);
        }
        return true;
      });

      // calculando monto
      // let monto = 0;
      const fechaPrestamo = moment(prestamoParam.fechaPrestamo, 'DD-MM-YYYY');
      const fechaDevolucion = moment(prestamoParam.fechaDevolucion, 'DD-MM-YYYY');
      const diasDiferencia = moment.duration(fechaDevolucion.diff(fechaPrestamo)).asDays();
      const precioDia = await precioModel.findOne({ dia: diasDiferencia });
      if (!precioDia) {
        throw new CodeError('el rando de días es mayor al permitido', 0, 404);
      }
      const monto = precioDia.costo * peliculas.length;
      // calculando descuento
      const descuento = await descuentoModel.findOne(
        {
          cantidad:
          {
            $lte: peliculas.length,
          },
        })
        .sort(
          {
            cantidad: -1,
          })
        .limit(1);

      let montoFinal = monto;
      // calculando montoFinal
      if (descuento.descuento !== 0) {
        montoFinal = monto - (monto * descuento.descuento);
      }
      // insertando prestamo:
      prestamoParam.monto = monto;
      prestamoParam.descuento = descuento.descuento;
      prestamoParam.montoFinal = montoFinal;
      const prestamo = await prestamoModel(prestamoParam).save();

      // actualizando stock de peliculas
      await Promise.all(peliculas.map(async (element) => {
        const nuevoStock = element.unidades - 1;
        await videoModel.findOneAndUpdate({ _id: element._id }, { $set: { unidades: nuevoStock } });
      }));

      return res.status(200).json(prestamo);
    } catch (error) {
      return mensajeError(res, error, error.errorCode);
    }
  },

  eliminarPrestamo: async (req, res) => {
    try {
      const idPrestamoParam = req.params.idPrestamo;
      const verificarExistencia = await prestamoModel.findById(idPrestamoParam);

      if (!verificarExistencia) {
        throw new CodeError('El prestamo no se encuentra registrado.', 0, 404);
      }

      if (verificarExistencia.estado === 'Conectado') {
        throw new CodeError('No se puede eliminar ese prestamo.', 0, 400);
      }

      await prestamoModel.remove({ _id: idPrestamoParam });
      res.status(204).json();
    } catch (error) {
      mensajeError(res, error, 400);
    }
  },

  editarPrestamo: async (req, res) => {
    try {
      await schemaHelper.validate(prestamoPatchSchema, req.body);
      const prestamo = await prestamoModel.findOne({ _id: req.params.idPrestamo }).exec();
      if (prestamo === null) {
        throw new CodeError('el prestamo no se encuentra registrado', 0, 404);
      }
      if (prestamo.estado === 'cerrado') {
        throw new CodeError('el prestamo ya se encuentra cerrado', 0, 404);
      }
      await prestamoModel.findOneAndUpdate({ _id: req.params.idPrestamo }, { $set: req.body });
      const nuevoPrestamo = await prestamoModel.findOne({ _id: req.params.idPrestamo });
      // actualizando stock de las peliculas
      await Promise.all(prestamo.peliculas.map(async (element) => {
        await videoModel.findOneAndUpdate({ _id: element }, { $inc: { unidades: 1 } });
      }));

      return res.status(200).json(nuevoPrestamo);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
  buscarPrestamoDetalles: async (req, res) => {
    try {
      await schemaHelper.validate(prestamoPatchSchema, req.body);
      if (mongoose.Types.ObjectId.isValid(req.params.idPrestamo) === false) {
        throw new CodeError(`ID de prestamo ${req.params.idPrestamo} no es un ID válido`, 0, 404);
      }
      const prestamo = await prestamoModel.findOne({ _id: req.params.idPrestamo }).exec();
      if (prestamo === null) {
        throw new CodeError('el prestamo no se encuentra registrado', 0, 404);
      }

      const detalles = await prestamoModel.aggregate([
        {
          $match: { _id: mongoose.Types.ObjectId(req.params.idPrestamo) },
        },
        {
          $lookup: {
            from: 'clientes',
            localField: 'cliente',
            foreignField: '_id',
            as: 'cliente',
          },
        },
        {
          $unwind: '$cliente',
        },
        {
          $project: {
            _id: 1,
            fechaDevolucion: 1,
            estado: 1,
            monto: 1,
            montoFinal: 1,
            descuento: 1,
            cliente: '$cliente.nombre',
            celular: '$cliente.celular',
            fechaNacimiento: '$cliente.fechaNacimiento',
            peliculas: 1,
          },
        },
        {
          $unwind: '$peliculas',
        },
        {
          $lookup: {
            from: 'videos',
            localField: 'peliculas',
            foreignField: '_id',
            as: 'peliculasDetalles',
          },
        },
        {
          $unwind: '$peliculasDetalles',
        },
        {
          $project: {
            _id: '$_id',
            fechaDevolucion: 1,
            cliente: 1,
            estado: 1,
            monto: 1,
            montoFinal: 1,
            descuento: 1,
            fechaNacimiento: 1,
            'pelicula.titulos': '$peliculasDetalles.titulos',
            'pelicula.actores': '$peliculasDetalles.actores',
            'pelicula.anio': '$peliculasDetalles.anio',
          },
        },
      ]);

       return res.status(200).json(detalles);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
};
