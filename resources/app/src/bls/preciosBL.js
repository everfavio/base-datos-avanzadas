const mongoose = require('mongoose');
const { precioModel } = require('../models');
const { mensajeError } = require('../utils/handleResponse');
const logger = require('../helpers/logger');
const schemaHelper = require('../helpers/schema_helper');
const precioPostSchema = require('../schemas/precioPostSchema');
const precioPatchSchema = require('../schemas/precioPatchSchema');
const CodeError = require('../errors/code_error');

module.exports = {
  listarPrecios: async (req, res) => {
    try {
      const paginas = req.query.pagina ? parseInt(req.query.pagina, 10) : 1;
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;
      const listaPreciosResult = await precioModel.paginate({}, { page: paginas, limit: limite });
      logger.info(`[__filename|]${listaPreciosResult}`, 'retornandoDatos');
      res.status(200).json({
        precios: listaPreciosResult.docs,
        totalPrecios: listaPreciosResult.total,
        limite: listaPreciosResult.limit,
        pagina: listaPreciosResult.page,
        totalPaginas: listaPreciosResult.pages,
      });
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  buscarPrecio: async (req, res) => {
    try {
      let query = {};
      const idPrecioParam = req.params.idPrecio;
      if (mongoose.Types.ObjectId.isValid(idPrecioParam) === false) {
        query = {
          dia: idPrecioParam,
        };
      } else {
        query = {
          _id: idPrecioParam,
        };
      }
      const resultado = await precioModel.findOne(query);
      if (resultado !== null) {
        res.status(200).json(resultado);
      }
      throw new CodeError('el precio no se encuentra registrado', 0, 404);
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  crearPrecio: async (req, res) => {
    try {
      await schemaHelper.validate(precioPostSchema, req.body);
      const precioParam = req.body;
      const verificarExistencia = await precioModel.findOne({ dia: precioParam.dia });
      if (verificarExistencia !== null) {
         throw new CodeError('el precio para el dia ya se encuentra registrado', 0, 400);
      }
      const resultado = await precioModel(precioParam).save();
      return res.status(200).json(resultado);
    } catch (error) {
      return mensajeError(res, error, error.errorCode);
    }
  },

  eliminarPrecio: async (req, res) => {
    try {
      const idPrecioParam = req.params.idPrecio;
      const verificarExistencia = await precioModel.findById(idPrecioParam);

      if (!verificarExistencia) {
        throw new CodeError('El precio no se encuentra registrado.', 0, 404);
      }

      if (verificarExistencia.estado === 'Conectado') {
        throw new CodeError('No se puede eliminar ese precio.', 0, 400);
      }

      await precioModel.remove({ _id: idPrecioParam });
      res.status(204).json();
    } catch (error) {
      mensajeError(res, error, 400);
    }
  },

  editarPrecio: async (req, res) => {
    try {
      await schemaHelper.validate(precioPatchSchema, req.body);
      const precio = await precioModel.findOne({ _id: req.params.idPrecio }).exec();
      if (precio === null) {
        throw new CodeError('el precio no se encuentra registrado', 0, 404);
      }
      await precioModel.findOneAndUpdate({ _id: req.params.idPrecio }, { $set: req.body });
      const nuevoPrecio = await precioModel.findOne({ _id: req.params.idPrecio });
      return res.status(200).json(nuevoPrecio);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
};
