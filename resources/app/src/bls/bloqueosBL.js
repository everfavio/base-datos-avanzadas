const mongoose = require('mongoose');
const { bloqueoModel } = require('../models');
const { mensajeError } = require('../utils/handleResponse');
const logger = require('../helpers/logger');
const schemaHelper = require('../helpers/schema_helper');
const bloqueoPostSchema = require('../schemas/bloqueoPostSchema');
const bloqueoPatchSchema = require('../schemas/bloqueoPatchSchema');
const CodeError = require('../errors/code_error');

module.exports = {
  listarBloqueos: async (req, res) => {
    try {
      const paginas = req.query.pagina ? parseInt(req.query.pagina, 10) : 1;
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;
      const listaBloqueosResult = await bloqueoModel.paginate({}, { page: paginas, limit: limite });
      logger.info(`[__filename|]${listaBloqueosResult}`, 'retornandoDatos');
      res.status(200).json({
        bloqueos: listaBloqueosResult.docs,
        totalBloqueos: listaBloqueosResult.total,
        limite: listaBloqueosResult.limit,
        pagina: listaBloqueosResult.page,
        totalPaginas: listaBloqueosResult.pages,
      });
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  buscarBloqueo: async (req, res) => {
    try {
      let query = {};
      const idBloqueoParam = req.params.idBloqueo;
      if (mongoose.Types.ObjectId.isValid(idBloqueoParam) === false) {
        query = {
          dia: idBloqueoParam,
        };
      } else {
        query = {
          _id: idBloqueoParam,
        };
      }
      const resultado = await bloqueoModel.findOne(query);
      if (resultado !== null) {
        res.status(200).json(resultado);
      }
      throw new CodeError('el bloqueo no se encuentra registrado', 0, 404);
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },
  crearBloqueo: async (req, res) => {
    try {
      await schemaHelper.validate(bloqueoPostSchema, req.body);
      const bloqueoParam = req.body;
      const resultado = await bloqueoModel(bloqueoParam).save();
      return res.status(200).json(resultado);
    } catch (error) {
      return mensajeError(res, error, error.errorCode);
    }
  },

  eliminarBloqueo: async (req, res) => {
    try {
      const idBloqueoParam = req.params.idBloqueo;
      const verificarExistencia = await bloqueoModel.findById(idBloqueoParam);

      if (!verificarExistencia) {
        throw new CodeError('El bloqueo no se encuentra registrado.', 0, 404);
      }

      if (verificarExistencia.estado === 'Conectado') {
        throw new CodeError('No se puede eliminar ese bloqueo.', 0, 400);
      }

      await bloqueoModel.remove({ _id: idBloqueoParam });
      res.status(204).json();
    } catch (error) {
      mensajeError(res, error, 400);
    }
  },

  editarBloqueo: async (req, res) => {
    try {
      await schemaHelper.validate(bloqueoPatchSchema, req.body);
      const bloqueo = await bloqueoModel.findOne({ _id: req.params.idBloqueo }).exec();
      if (bloqueo === null) {
        throw new CodeError('el bloqueo no se encuentra registrado', 0, 404);
      }
      await bloqueoModel.findOneAndUpdate({ _id: req.params.idBloqueo }, { $set: req.body });
      const nuevoBloqueo = await bloqueoModel.findOne({ _id: req.params.idBloqueo });
      return res.status(200).json(nuevoBloqueo);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
};
