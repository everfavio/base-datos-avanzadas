const mongoose = require('mongoose');
const { videoModel, prestamoModel } = require('../models');
const { mensajeError } = require('../utils/handleResponse');
const logger = require('../helpers/logger');
const schemaHelper = require('../helpers/schema_helper');
const videoPostSchema = require('../schemas/videoPostSchema');
const videoPatchSchema = require('../schemas/videoPatchSchema');
const CodeError = require('../errors/code_error');

module.exports = {
  listarVideos: async (req, res) => {
    try {
      let listaVideosResult;
      const paginas = req.query.pagina ? parseInt(req.query.pagina, 10) : 1;
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;
      const { keyword } = req.query;

      if (keyword === null || keyword === undefined || keyword === '') {
        listaVideosResult = await videoModel.paginate({}, { page: paginas, limit: limite });
      } else {
        listaVideosResult = await videoModel.paginate(
          {
          $or: [
            { codigo: new RegExp(keyword.toUpperCase()) },
            ],
          },
          { page: paginas, limit: limite },
        );
      }
      logger.info(`[__filename|]${listaVideosResult}`, 'retornandoDatos');
      res.status(200).json({
        videos: listaVideosResult.docs,
        totalVideos: listaVideosResult.total,
        limite: listaVideosResult.limit,
        pagina: listaVideosResult.page,
        totalPaginas: listaVideosResult.pages,
      });
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },

  buscarVideo: async (req, res) => {
    try {
      let query = {};
      const idVideoParam = req.params.idVideo;
      if (mongoose.Types.ObjectId.isValid(idVideoParam) === false) {
        query = {
          codigo: idVideoParam,
        };
      } else {
        query = {
          _id: idVideoParam,
        };
      }
      const resultado = await videoModel.findOne(query);
      if (resultado !== null) {
        res.status(200).json(resultado);
      }
      throw new CodeError('el video no se encuentra registrado', 0, 404);
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },

  crearVideo: async (req, res) => {
    try {
      await schemaHelper.validate(videoPostSchema, req.body);
      const videoParam = req.body;
      const verificarExistencia = await videoModel.findOne({ codigo: videoParam.codigo });
      if (verificarExistencia !== null) {
         throw new CodeError('La película ya se encuentra registrada', 0, 400);
      }
      const resultado = await videoModel(videoParam).save();
      return res.status(200).json(resultado);
    } catch (error) {
      return mensajeError(res, error, error.errorCode);
    }
  },

  eliminarVideo: async (req, res) => {
    try {
      const idVideoParam = req.params.idVideo;
      const verificarExistencia = await videoModel.findById(idVideoParam);

      if (!verificarExistencia) {
        throw new CodeError('El video no se encuentra registrado.', 0, 404);
      }

      if (verificarExistencia.estado === 'Conectado') {
        throw new CodeError('No se puede eliminar ese video.', 0, 400);
      }

      await videoModel.remove({ _id: idVideoParam });
      res.status(204).json();
    } catch (error) {
      mensajeError(res, error, 400);
    }
  },

  editarVideo: async (req, res) => {
    try {
      await schemaHelper.validate(videoPatchSchema, req.body);
      const video = await videoModel.findOne({ _id: req.params.idVideo }).exec();
      if (video === null) {
        throw new CodeError('el video no se encuentra registrado', 0, 404);
      }
      await videoModel.findOneAndUpdate({ _id: req.params.idVideo }, { $set: req.body });
      const nuevoVideo = await videoModel.findOne({ _id: req.params.idVideo });
      return res.status(200).json(nuevoVideo);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
  // getRanking
  getRanking: async (req, res) => {
    try {
      // await schemaHelper.validate(videoPatchSchema, req.body);
      // const video = await videoModel.findOne({ _id: req.params.idVideo }).exec();
      //
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;

      const ranking = await prestamoModel.aggregate([
        {
          $unwind: '$peliculas',
        },
        {
          $group: {
              _id: '$peliculas',
              cantidad: { $sum: 1 },
              fechas: {
                $push: '$fechaPrestamo',
            },
          },
        },
         {
           $sort:
          {
            cantidad: -1,
          },
        },
         {
          $lookup: {
                    from: 'videos',
                    localField: '_id',
                    foreignField: '_id',
                    as: 'videosDetalles',
          },
        },
        {
          $project: {
            _id: '$_id',
            fechas: '$fechas',
            videoTitulo: '$videosDetalles.titulos',
            cantidad: 1,
          },
        },
        {
          $limit: limite,
        },
    ]);

      // await videoModel.findOneAndUpdate({ _id: req.params.idVideo }, { $set: req.body });
      // const nuevoVideo = await videoModel.findOne({ _id: req.params.idVideo });
      return res.status(200).json(ranking);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
};
