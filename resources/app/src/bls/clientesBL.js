const mongoose = require('mongoose');
const { clienteModel } = require('../models');
const { mensajeError } = require('../utils/handleResponse');
const logger = require('../helpers/logger');
const schemaHelper = require('../helpers/schema_helper');
const clientePostSchema = require('../schemas/clientePostSchema');
const clientePatchSchema = require('../schemas/clientePatchSchema');
const CodeError = require('../errors/code_error');

module.exports = {
  listarClientes: async (req, res) => {
    try {
      let listaClientesResult;
      const paginas = req.query.pagina ? parseInt(req.query.pagina, 10) : 1;
      const limite = req.query.limite ? parseInt(req.query.limite, 10) : 10;
      const { keyword } = req.query;
      if (keyword === null || keyword === undefined || keyword === '') {
        listaClientesResult = await clienteModel.paginate({}, { page: paginas, limit: limite });
      } else {
        listaClientesResult = await clienteModel.paginate(
          {
          $or: [
            { siglaCliente: new RegExp(keyword.toUpperCase()) },
            { desCliente: new RegExp(keyword) },
            ],
          },
          { page: paginas, limit: limite },
        );
      }
      logger.info(`[__filename|]${listaClientesResult}`, 'retornandoDatos');
      res.status(200).json({
        clientes: listaClientesResult.docs,
        totalClientes: listaClientesResult.total,
        limite: listaClientesResult.limit,
        pagina: listaClientesResult.page,
        totalPaginas: listaClientesResult.pages,
      });
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },

  buscarCliente: async (req, res) => {
    try {
      let query = {};
      const idClienteParam = req.params.idCliente;
      if (mongoose.Types.ObjectId.isValid(idClienteParam) === false) {
        query = {
          codigo: idClienteParam,
        };
      } else {
        query = {
          _id: idClienteParam,
        };
      }
      const resultado = await clienteModel.findOne(query);
      if (resultado !== null) {
        res.status(200).json(resultado);
      }
      throw new CodeError('el cliente no se encuentra registrado', 0, 404);
    } catch (error) {
      logger.error(`[__filename|]${error}`, { error });
      mensajeError(res, error, 400);
    }
  },

  crearCliente: async (req, res) => {
    try {
      await schemaHelper.validate(clientePostSchema, req.body);
      const clienteParam = req.body;
      const verificarExistencia = await clienteModel.findOne({ nroDocumento: clienteParam.nroDocumento });
      if (verificarExistencia !== null) {
         throw new CodeError('El número de documento ya se encuentra registrado', 0, 400);
      }
      clienteParam.coordenadas = {
        type: 'Point',
        coordinates: [clienteParam.coordenadas.latitud, clienteParam.coordenadas.longitud],
      };
      const resultado = await clienteModel(clienteParam).save();
      return res.status(200).json(resultado);
    } catch (error) {
      return mensajeError(res, error, error.errorCode);
    }
  },

  eliminarCliente: async (req, res) => {
    try {
      const idClienteParam = req.params.idCliente;
      const verificarExistencia = await clienteModel.findById(idClienteParam);

      if (!verificarExistencia) {
        throw new CodeError('El cliente no se encuentra registrado.', 0, 404);
      }

      if (verificarExistencia.estado === 'Conectado') {
        throw new CodeError('No se puede eliminar ese cliente.', 0, 400);
      }

      await clienteModel.remove({ _id: idClienteParam });
      res.status(204).json();
    } catch (error) {
      mensajeError(res, error, 400);
    }
  },

  editarCliente: async (req, res) => {
    try {
      await schemaHelper.validate(clientePatchSchema, req.body);
      const cliente = await clienteModel.findOne({ _id: req.params.idCliente }).exec();
      if (cliente === null) {
        throw new CodeError('el cliente no se encuentra registrado', 0, 404);
      }
      const clienteParam = req.body;
      if (clienteParam.coordenadas) {
        clienteParam.coordenadas = {
          type: 'Point',
          coordinates: [clienteParam.coordenadas.latitud, clienteParam.coordenadas.longitud],
        };
      }
      await clienteModel.findOneAndUpdate({ _id: req.params.idCliente }, { $set: clienteParam });
      const nuevoCliente = await clienteModel.findOne({ _id: req.params.idCliente });
      return res.status(200).json(nuevoCliente);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
  // historial
  historial: async (req, res) => {
    try {
      if (mongoose.Types.ObjectId.isValid(req.params.idCliente) === false) {
        throw new CodeError(`ID de cliente ${req.params.idCliente} no es un ID válido`, 0, 404);
      }
      await schemaHelper.validate(clientePatchSchema, req.body);
      const cliente = await clienteModel.findOne({ _id: req.params.idCliente }).exec();
      if (cliente === null) {
        throw new CodeError('el cliente no se encuentra registrado', 0, 404);
      }

      const clienteHistorial = await clienteModel.aggregate([
        {
          $match: {
            _id: mongoose.Types.ObjectId(req.params.idCliente),
          },
        },
        {
          $lookup: {
            from: 'prestamos',
            localField: '_id',
            foreignField: 'cliente',
            as: 'prestamosJoin',
          },
        },
        {
          $unwind: '$prestamosJoin',
        },
        {
          $project: {
            _id: 1,
            nombre: 1,
            fechaPrestamo: '$prestamosJoin.fechaPrestamo',
            peliculas: '$prestamosJoin.peliculas',
          },
        },
        {
          $unwind: '$peliculas',
        },
         {
          $lookup: {
                    from: 'videos',
                    localField: 'peliculas',
                    foreignField: '_id',
                    as: 'videosDetalles',
          },
        },
        {
          $unwind: '$videosDetalles',
        },
        {
          $project: {
            _id: '$_id',
            cliente: '$nombre',
            fechaPrestamo: '$fechaPrestamo',
            titulos: '$videosDetalles.titulos',
            videoId: '$videosDetalles._id',
          },
        },
         {
            $sort: {
              fechaPrestamo: -1,
            },
          },
        {
          $group: {
              _id: '$videoId',
              count: { $sum: 1 },
              fechaPrestamo:
              {
                $push: '$fechaPrestamo',
              },
          },
        },
        {
          $lookup: {
            from: 'videos',
            localField: '_id',
            foreignField: '_id',
            as: 'tituloVideo',
          },
        },
        {
          $unwind: '$tituloVideo',
        },
          {
            $project: {
            _id: '$_id',
            fechaPrestamo: '$fechaPrestamo',
            titulos: '$tituloVideo.titulos',
            count: 1,
          },
        },
      ]);

      return res.status(200).json(clienteHistorial);
    } catch (error) {
      return mensajeError(res, error, 400);
    }
  },
};
