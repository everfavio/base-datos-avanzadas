const mongoose = require('mongoose');
const logger = require('../helpers/logger');
const configs = require('../configs/database');
const mongoosePaginate = require('mongoose-paginate');

const configuracionesDB = configs;
module.exports = app => {
  mongoosePaginate.paginate.options = {
    lean: true,
  };
  let url = '';
  if (configuracionesDB.user === '' || configuracionesDB.user === null || configuracionesDB.user === undefined) {
    url = `mongodb://${configuracionesDB.host}:${configuracionesDB.port}/${configuracionesDB.base}`;
  } else {
    url = `mongodb://${configuracionesDB.user}:${configuracionesDB.pass}@${configuracionesDB.host}:${
      configuracionesDB.port
    }/${configuracionesDB.base}`;
  }
  mongoose.set('debug', true);
  mongoose.set('useNewUrlParser', true);
  mongoose.set('useUnifiedTopology', true);

  mongoose.connect(
    url,
    (err, clientParam) => {
      app.client = clientParam;
      if (err) {
        logger.error(`[${__filename}|middleware] No se puede conectar a mongo`, {
          errorMessage: err.message,
          errorStack: err.stack,
        });
        process.exit(0);
      }
      logger.info(`[${__filename}|middleware] Conectado a mongo`);
      app.eventos.emit('mongoConectado');
    },
  );

  process.on('SIGINT', () => {
    app.client.close(() => {
      logger.info(`[${__filename}|middleware] Cerrando la conexion a mongo`);
      process.exit(0);
    });
  });
};
