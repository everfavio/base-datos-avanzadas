const express = require('express');
const middlewaresBefore = require('./middlewares/before_all');
const middlewaresAfter = require('./middlewares/after_routes');
const routes = require('../routes');
const database = require('./db');
const EmisorEventos = require('events');
const cors = require('cors');

const app = express();
app.use(cors());
app.eventos = new EmisorEventos();

middlewaresBefore(app);

routes(app);

middlewaresAfter(app);

database(app);

module.exports = app;
