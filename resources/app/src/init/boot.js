const app = require('./server');
const logger = require('../helpers/logger');

const env = process.env.NODE_ENV;

module.exports = () => {
  const port = app.get('port') || 1800;
  app.listen(port, () => {
    logger.log('info', `[${env}]Servidor escuchando en el puerto  ${port}...`);
  });
};
