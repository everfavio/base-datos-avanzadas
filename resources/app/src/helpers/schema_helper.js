const Ajv = require('ajv');
const { plugin } = require('ajv-moment');
const moment = require('moment');
const localizeAjv = require('ajv-i18n');
const CodeError = require('../errors/code_error');
const localizeSchema = require('./localize_schema_validation');

module.exports = {
  validate: (schema, datos) => {
    const ajv = new Ajv();
    plugin({ ajv, moment });
    const validate = ajv.compile(schema);
    const valid = validate(datos);
    if (!valid) {
      localizeAjv.es(validate.errors);
      throw new CodeError(localizeSchema(ajv.errorsText(validate.errors, ''), schema), 0, 422);
    }
  },
};
