module.exports = {
  mensajeExito: (res, mensajeExitoParam, codigo, datos) => {
    const codigoEnviar = codigo || 200;
    return res.status(codigoEnviar).json({
      finalizado: true,
      mensaje: mensajeExitoParam,
      datos: datos || null,
    });
  },
  mensajeError: (res, mensajeErrorParam, codigo) => {
    const codigoEnviar = codigo || 400;
    if (typeof mensajeErrorParam === 'object' && mensajeErrorParam.errors) {
      mensajeErrorParam = mensajeErrorParam.errors.map(x => x.message).join(', ');
    } else if (mensajeErrorParam.message) {
      mensajeErrorParam = mensajeErrorParam.message;
    }
    return res.status(codigoEnviar).json({
      finalizado: false,
      mensaje: mensajeErrorParam,
      datos: null,
    });
  },
};
