const express = require('express');
// const logger = require('../helpers/logger');
const { bloqueosBL } = require('../bls');

const router = express.Router();

router.get('/bloqueos', bloqueosBL.listarBloqueos);
router.post('/bloqueos', bloqueosBL.crearBloqueo);
router.get('/bloqueos/:idBloqueo', bloqueosBL.buscarBloqueo);
router.patch('/bloqueos/:idBloqueo', bloqueosBL.editarBloqueo);
router.delete('/bloqueos/:idBloqueo', bloqueosBL.eliminarBloqueo);

module.exports = router;
