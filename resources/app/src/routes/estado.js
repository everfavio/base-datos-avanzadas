const express = require('express');
const propiedades = require('../../package.json');
const fs = require('fs');

const router = express.Router();

router.route('/estado')
  .get((req, res) => {
    res.json({ estado: 'aplicación VideoClub v1 disponible' });
  });

router.route('/despliegue')
  .get((req, res) => {
    res.json({ version: `${propiedades.version}` });
  });

router.route('/openapi')
  .get((req, res) => {
    res.type('txt/yaml; charset=foobar');
    return fs.readFile('openapi.yaml', (e, data) => {
      if (e) throw e;
      res.send(data);
    });
  });
module.exports = router;
