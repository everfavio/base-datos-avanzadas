const appBase = require('../configs');
const propiedades = require('../../package.json');
const estado = require('./estado');
const videosRT = require('./videosRT');
const clientesRT = require('./clientesRT');
const preciosRT = require('./preciosRT');
const descuentosRT = require('./descuentosRT');
const bloqueosRT = require('./bloqueosRT');
const prestamosRT = require('./prestamosRT');


const armarRutas = app => {
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, estado);
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, videosRT);
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, clientesRT);
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, preciosRT);
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, descuentosRT);
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, bloqueosRT);
  app.use(`${appBase.appBase.baseUrl}/v${propiedades.datosServicio.version}`, prestamosRT);
};

module.exports = armarRutas;
