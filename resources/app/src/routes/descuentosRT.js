const express = require('express');
// const logger = require('../helpers/logger');
const { descuentosBL } = require('../bls');

const router = express.Router();

router.get('/descuentos', descuentosBL.listarDescuentos);
router.post('/descuentos', descuentosBL.crearDescuento);
router.get('/descuentos/:idDescuento', descuentosBL.buscarDescuento);
router.patch('/descuentos/:idDescuento', descuentosBL.editarDescuento);
router.delete('/descuentos/:idDescuento', descuentosBL.eliminarDescuento);

module.exports = router;
