const express = require('express');
// const logger = require('../helpers/logger');
const { preciosBL } = require('../bls');

const router = express.Router();

router.get('/precios', preciosBL.listarPrecios);
router.post('/precios', preciosBL.crearPrecio);
router.get('/precios/:idPrecio', preciosBL.buscarPrecio);
router.patch('/precios/:idPrecio', preciosBL.editarPrecio);
router.delete('/precios/:idPrecio', preciosBL.eliminarPrecio);

module.exports = router;
