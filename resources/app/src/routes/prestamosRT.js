const express = require('express');
const { prestamosBL } = require('../bls');

const router = express.Router();

router.get('/prestamos', prestamosBL.listarPrestamos);
router.post('/prestamos', prestamosBL.crearPrestamo);
router.get('/prestamos/:idPrestamo', prestamosBL.buscarPrestamo);
router.get('/prestamos/:idPrestamo/detalles', prestamosBL.buscarPrestamoDetalles);
router.patch('/prestamos/:idPrestamo', prestamosBL.editarPrestamo);
router.delete('/prestamos/:idPrestamo', prestamosBL.eliminarPrestamo);

module.exports = router;
