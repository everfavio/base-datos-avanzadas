const express = require('express');
// const logger = require('../helpers/logger');
const { clientesBL } = require('../bls');

const router = express.Router();

router.get('/clientes', clientesBL.listarClientes);
router.post('/clientes', clientesBL.crearCliente);
router.get('/clientes/:idCliente/historial', clientesBL.historial);
router.get('/clientes/:idCliente', clientesBL.buscarCliente);
router.patch('/clientes/:idCliente', clientesBL.editarCliente);
router.delete('/clientes/:idCliente', clientesBL.eliminarCliente);

module.exports = router;
