const express = require('express');
// const logger = require('../helpers/logger');
const { videosBL } = require('../bls');

const router = express.Router();

router.get('/videos', videosBL.listarVideos);
router.post('/videos', videosBL.crearVideo);
router.get('/videos/ranking', videosBL.getRanking);
router.get('/videos/:idVideo', videosBL.buscarVideo);
router.patch('/videos/:idVideo', videosBL.editarVideo);
router.delete('/videos/:idVideo', videosBL.eliminarVideo);

module.exports = router;
