const appBase = require('./app');
const database = require('./database');

module.exports = {
  appBase,
  database,
};
