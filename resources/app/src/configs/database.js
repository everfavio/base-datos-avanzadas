module.exports = {
  host: process.env.DB_HOST || '192.168.10.10',
  base: process.env.DB_BASE || 'videoclub',
  port: process.env.DB_PORT || '27017',
  user: process.env.DB_USER || '',
  pass: process.env.DB_PASS || '',
};

