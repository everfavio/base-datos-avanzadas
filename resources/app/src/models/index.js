const videoModel = require('./video');
const clienteModel = require('./cliente');
const precioModel = require('./precio');
const descuentoModel = require('./descuento');
const bloqueoModel = require('./bloqueo');
const prestamoModel = require('./prestamo');

module.exports = {
  videoModel,
  clienteModel,
  precioModel,
  descuentoModel,
  bloqueoModel,
  prestamoModel,
};
