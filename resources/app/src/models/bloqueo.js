const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const bloqueoSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    auto: true,
  },
  fecha: {
    type: String,
  },
  descripcion: {
    type: String,
  },
  estado: {
    type: String,
  },
});

mongoosePaginate(bloqueoSchema);
// bloqueoSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('bloqueo', bloqueoSchema, 'bloqueos');
