const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const prestamoSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    auto: true,
  },
  peliculas: [{
      type: mongoose.Types.ObjectId,
  }],
  cliente: {
    type: mongoose.Types.ObjectId,
  },
  fechaPrestamo: {
    type: String,
  },
  fechaDevolucion: {
    type: String,
  },
  monto: {
    type: Number,
  },
  descuento: {
    type: Number,
  },
  montoFinal: {
    type: Number,
  },
  estado: {
    type: String,
  },
});

mongoosePaginate(prestamoSchema);
// prestamoSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('prestamo', prestamoSchema, 'prestamos');
