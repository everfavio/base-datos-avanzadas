const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const descuentoSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    auto: true,
  },
  cantidad: {
    type: Number,
  },
  descuento: {
    type: Number,
  },
});

mongoosePaginate(descuentoSchema);
// descuentoSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('descuento', descuentoSchema, 'descuentos');
