const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const clienteSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    auto: true,
  },
  nombre: {
    type: String,
  },
  celular: {
    type: String,
  },
  email: {
    type: String,
  },
  fechaNacimiento: {
    type: String,
  },
  nroDocumento: {
    type: String,
  },
  direccion: {
    type: String,
  },
  coordenadas: {
    type: {
      type: String, default: 'Point',
    },
    coordinates: {
      type: [Number], default: [0, 0],
    },
  },
  idBloqueo: [{
    type: mongoose.Types.ObjectId,
  }],
});

mongoosePaginate(clienteSchema);
// clienteSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('cliente', clienteSchema, 'clientes');
