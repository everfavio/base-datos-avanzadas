const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const precioSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    auto: true,
  },
  dia: {
    type: Number,
  },
  costo: {
    type: Number,
  },
});

mongoosePaginate(precioSchema);
// precioSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('precio', precioSchema, 'precios');
