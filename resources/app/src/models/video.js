const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const { Schema } = mongoose;

const videoSchema = new Schema({
  _id: {
    type: mongoose.Types.ObjectId,
    auto: true,
  },
  codigo: {
    type: String,
  },
  titulos: [
    {
      principal: String,
      titulo: String,
    },
  ],
  generos: [
    {
      type: String,
    },
  ],
  anio: {
    type: Number,
  },
  nominaciones: [
    {
      type: String,
    },
  ],
  oscars: [
    {
      type: String,
    },
  ],
  actores: [
    {
      type: String,
    },
  ],
  unidades: {
    type: Number,
  },
});

mongoosePaginate(videoSchema);
// videoSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('video', videoSchema, 'videos');
