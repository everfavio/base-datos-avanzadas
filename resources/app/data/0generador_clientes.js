const faker = require('faker');
const fs = require('fs');

const generateEmployees = () => {
  const employees = [];
  // eslint-disable-next-line no-plusplus
  for (let id = 1; id <= 30; id++) {
    // const firstName = faker.name.firstName();
    // const lastName = faker.name.lastName();
    // const email = faker.internet.email();
    const cliente = {
      nombre: faker.name.findName(),
      celular: faker.datatype.number({ min: 6000000, max: 7999999 }),
      email: faker.internet.email(),
      fechaNacimiento: faker.date.between('1989-01-01', '2010-01-05'),
      nroDocumento: faker.datatype.number({ min: 1000000, max: 9999999 }),
      direccion: faker.address.streetAddress(),
      coordenadas: {
        type: 'Point',
        coordinates: [
          Number(faker.address.latitude()),
          Number(faker.address.longitude()),
        ],
      },
      idbloqueo: '',
    };
    employees.push(cliente);
  }
  return { employees };
};

module.exports = generateEmployees;

const dataObj = generateEmployees();
fs.writeFileSync('clientes.json', JSON.stringify(dataObj, null, '\t'));
