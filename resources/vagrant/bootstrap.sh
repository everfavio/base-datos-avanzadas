#!/usr/bin/env bash
# instalamos los paquetes necesario
export DEBIAN_FRONTEND=noninteractive
sudo export DEBIAN_FRONTEND=noninteractive

sudo apt-get update
sudo apt-get -y install net-tools
sudo apt-get -y install build-essential
sudo apt-get -y install vim
sudo apt-get -y install dirmngr
sudo apt-get -y install gnupg
sudo apt-get -y install apt-transport-https
sudo apt-get -y install software-properties-common
sudo apt-get -y install ca-certificates

# instalando mongodb
curl -fsSL https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
sudo echo  'deb https://repo.mongodb.org/apt/debian buster/mongodb-org/4.2 main' >> /etc/apt/sources.list
sudo apt-get update

sudo apt-get install mongodb-org -y
sudo systemctl start mongod
sudo systemctl daemon-reload
sudo systemctl enable mongod --now
sudo systemctl restart mongod
sudo systemctl status mongod
mongo --eval 'db.runCommand({ connectionStatus: 1 })'

#configurando ssh user
sed -i "/^[^#]*PasswordAuthentication[[:space:]]no/c\PasswordAuthentication yes" /etc/ssh/sshd_config
systemctl restart sshd.service
## creamos el usuario
useradd -m -s /bin/bash mongo
## establecemos la contraseña ('usuario:password')
echo 'mongo:mongo' | chpasswd
## agregamos al nuevo usuario al grupo sudoers
echo 'mongo  ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
## generamos las llaves ssh para habilitar la autenticación ssh
sudo su mongo
cd
ssh-keygen -t dsa -N "mongo" -f "$HOME/.ssh/id_rsa"
exit
