## agregando hosts
sudo echo '127.0.0.1  node1' >> /etc/hosts
sudo echo '192.168.10.10  master1' >> /etc/hosts
sudo echo '192.168.10.12  node2' >> /etc/hosts
sudo echo '192.168.10.9  appNode' >> /etc/hosts

sudo sed -i '24 c  \ \ bindIp: ::,0.0.0.0' /etc/mongod.conf
sudo sed -i '35 c  replication:\n \ replSetName: "rs0"' /etc/mongod.conf

sudo systemctl restart mongod
