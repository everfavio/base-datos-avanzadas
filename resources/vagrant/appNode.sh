
## agregando hosts
sudo echo '127.0.0.1  appNode' >> /etc/hosts
sudo echo '192.168.10.10  master1' >> /etc/hosts
sudo echo '192.168.10.11  node1' >> /etc/hosts
sudo echo '192.168.10.12  node2' >> /etc/hosts


sudo sed -i '24 c  \ \ bindIp: ::,0.0.0.0' /etc/mongod.conf

sudo systemctl restart mongod


sudo mongo -host 192.168.10.10  -port 27017 --eval "rs.initiate();"
## agregando los otros nodos como secundarios
sudo mongo -host 192.168.10.10  -port 27017 --eval "rs.add('192.168.10.11:27017')";
sudo mongo -host 192.168.10.10  -port 27017 --eval "rs.add('192.168.10.12:27017')";

# habilitando query shell en los nodos secundarios
sudo mongo -host 192.168.10.11 -port 27017 --eval 'rs.secondaryOk();';
sudo mongo -host 192.168.10.12 -port 27017 --eval 'rs.secondaryOk();';

## clonando la aplicación
