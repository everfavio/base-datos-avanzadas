## MAESTRÍA EN CIENCIA DE DATOS V.2
# PROYECTO FINAL DEL MÓDULO BASES DE DATOS AVANZADAS

#### Docente: Ernesto Campohermoso
#### Estudiante: Ever Favio Argollo Ticona
#### La Paz, Mayo del 2021


![](./docs/img/img1.jpg)


### Indice
[0. Enunciado y Reglas](./docs/0.reglas.md)

[1. Configuración de un clúster MongoDB](./docs/1.cluster.md)

[2. Diccionario de la base de datos](./docs/2.documentacion.md)

[3. Llenado de la base de datos](./docs/3.cargado.md)

[4. Uso de la aplicación](./docs/4.implementacion.md)

[5. Consultas a la base de datos](./docs/5.consultas.md)

